package com.customermanagementservice.dtos;

import com.customermanagementservice.enums.AccountType;
import com.customermanagementservice.models.Customers;
import lombok.Builder;
import lombok.Data;


@Builder
@Data
public class AccountDto {

    /**
     * Account unique identifier
     */
    private Long accountId;

    private String accountName;

    private AccountType accountType;

    private Customers customerId;

    private Long balance;
}
