package com.customermanagementservice.dtos;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class CustomerDtos {

    private Long customerId;

    private String firstName;

    private String lastName;
}
