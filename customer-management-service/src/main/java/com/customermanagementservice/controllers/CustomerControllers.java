package com.customermanagementservice.controllers;

import com.customermanagementservice.dtos.CustomerDtos;
import com.customermanagementservice.errors.CustomErrorException;
import com.customermanagementservice.services.CustomerServiceI;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customers")
public class CustomerControllers {
    private final CustomerServiceI customerService;

    /**
     * Endpoint to add a customer
     */
    @PostMapping
    public ResponseEntity<CustomerDtos> saveCustomer(@RequestBody CustomerDtos dto){
        CustomerDtos savedData = customerService.saveCustomer(dto);
        return new ResponseEntity<CustomerDtos> (savedData, HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<List<CustomerDtos>> findAllCustomers() {
        List<CustomerDtos> titles = customerService.findAll();
        return ResponseEntity.ok(titles);
    }


    @GetMapping("/{code}")
    public ResponseEntity<CustomerDtos> getCustomerById(@PathVariable("code") Long code) throws CustomErrorException {
        CustomerDtos customer = customerService.findCustomerById(code);
        return ResponseEntity.ok(customer);
    }

    @PutMapping("/{code}")
    public ResponseEntity<CustomerDtos> updateCustomer(
            @PathVariable("code") Long code,
            @RequestBody CustomerDtos dto) throws CustomErrorException{
            dto = customerService.updateCustomer(dto, code);
            return new ResponseEntity<CustomerDtos>(dto, HttpStatus.OK);
    }

    @DeleteMapping("/{code}")
    public ResponseEntity<CustomerDtos> DeleteCustomerByCode(
            @PathVariable("code") Long code) throws CustomErrorException {
        CustomerDtos dto = CustomerDtos.builder().build();
            customerService.deleteCustomer(code);
            return new ResponseEntity<CustomerDtos>(dto, HttpStatus.OK);

    }
}
