package com.customermanagementservice.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * Definition of all available account types
 */

@Getter
@AllArgsConstructor
public enum AccountType {

    DEBIT("D","Debit"),
    CREDIT("C","CREDIT");

    private String code;

    private String name;

    public static AccountType get(String statusCode){
        return Arrays.stream(values()).filter(status -> status.getCode().equals(statusCode) ).findFirst().orElse(null);
    }
}
