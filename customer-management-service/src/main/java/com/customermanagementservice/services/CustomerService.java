package com.customermanagementservice.services;

import com.customermanagementservice.dtos.CustomerDtos;
import com.customermanagementservice.errors.CustomErrorException;
import com.customermanagementservice.models.Customers;
import com.customermanagementservice.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CustomerService implements CustomerServiceI{
    private final CustomerRepository customerRepository;

    @Override
    public CustomerDtos saveCustomer(CustomerDtos dto){
        Customers customers = getCustomers(dto);
        customers = customerRepository.save(customers);
        return getCustomerDto(customers);
    }

    @Override
    public CustomerDtos updateCustomer(CustomerDtos dtos, Long id) throws CustomErrorException {
        if (!(customerRepository.existsByCustomerId(id))){
            throw new CustomErrorException("Customer with Id "+ id + " does not exist");
        }
        Customers customers = getCustomers(dtos);
        customers = customerRepository.save(customers);
        return getCustomerDto(customers);
    }

    @Override
    public List<CustomerDtos> findAll(){
        List<Customers> dto = customerRepository.findAll();
        return dto.stream().map(this::getCustomerDto).collect(Collectors.toList());
    }

    @Override
    public CustomerDtos findCustomerById(Long id) throws CustomErrorException {
        Customers address = customerRepository.findById(id).orElseThrow(()->
                new CustomErrorException("Address with id " +id+" does not exist"));
        return getCustomerDto(address);
    }

    @Override
    public void deleteCustomer(Long id) throws CustomErrorException {
        Customers address = customerRepository.findById(id).orElseThrow(()->
                new CustomErrorException("Address with id " +id+" does not exist"));
        customerRepository.delete(address);
    }

    private CustomerDtos getCustomerDto(Customers customers) {
        return CustomerDtos.builder()
                .customerId(customers.getCustomerId())
                .firstName(customers.getFirstName())
                .lastName(customers.getLastName())
                .build();
    }

    private Customers getCustomers(CustomerDtos dto) {
        return Customers.builder()
                .customerId(dto.getCustomerId())
                .firstName(dto.getFirstName())
                .lastName(dto.getLastName())
                .build();
    }
}
