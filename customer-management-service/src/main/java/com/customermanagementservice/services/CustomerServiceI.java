package com.customermanagementservice.services;

import com.customermanagementservice.dtos.CustomerDtos;
import com.customermanagementservice.errors.CustomErrorException;

import java.util.List;

public interface CustomerServiceI {
    CustomerDtos saveCustomer(CustomerDtos dto);
    public CustomerDtos updateCustomer(CustomerDtos dtos, Long id) throws CustomErrorException;
    public List<CustomerDtos> findAll();
    public CustomerDtos findCustomerById(Long id) throws CustomErrorException;
    public void deleteCustomer(Long id) throws CustomErrorException;
}
