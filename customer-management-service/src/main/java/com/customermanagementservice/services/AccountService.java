package com.customermanagementservice.services;

import com.customermanagementservice.dtos.AccountDto;
import com.customermanagementservice.errors.CustomErrorException;
import com.customermanagementservice.models.Accounts;
import com.customermanagementservice.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class AccountService {
//    private final AccountRepository accountRepository;
//
//    @Override
//    public AccountDto saveCustomer(AccountDto dto){
//        Accounts Accounts = getAccounts(dto);
//        Accounts = accountRepository.save(Accounts);
//        return getCustomerDto(Accounts);
//    }
//
//    @Override
//    public AccountDto updateCustomer(AccountDto dtos, Long id) throws CustomErrorException {
//        if (!(accountRepository.existsByAccountId(id))){
//            throw new CustomErrorException("Customer with Id "+ id + " does not exist");
//        }
//        Accounts Accounts = getAccounts(dtos);
//        Accounts = accountRepository.save(Accounts);
//        return getCustomerDto(Accounts);
//    }
//
//    @Override
//    public List<AccountDto> findAll(){
//        List<Accounts> dto = accountRepository.findAll();
//        return dto.stream().map(this::getCustomerDto).collect(Collectors.toList());
//    }
//
//    @Override
//    public AccountDto findCustomerById(Long id) throws CustomErrorException {
//        Accounts address = accountRepository.findById(id).orElseThrow(()->
//                new CustomErrorException("Address with id " +id+" does not exist"));
//        return getCustomerDto(address);
//    }
//
//    @Override
//    public void deleteCustomer(Long id) throws CustomErrorException {
//        Accounts address = accountRepository.findById(id).orElseThrow(()->
//                new CustomErrorException("Address with id " +id+" does not exist"));
//        accountRepository.delete(address);
//    }
//
//    private AccountDto getCustomerDto(Accounts Accounts) {
//        return AccountDto.builder()
//                .customerId(Accounts.getCustomerId())
//                .firstName(Accounts.getFirstName())
//                .lastName(Accounts.getLastName())
//                .build();
//    }
//
//    private Accounts getAccounts(AccountDto dto) {
//        return Accounts.builder()
//                .customerId(dto.getCustomerId())
//                .firstName(dto.getFirstName())
//                .lastName(dto.getLastName())
//                .build();
//    }
}
