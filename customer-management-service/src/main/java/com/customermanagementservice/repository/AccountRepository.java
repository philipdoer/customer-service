package com.customermanagementservice.repository;

import com.customermanagementservice.models.Accounts;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Accounts, Long> {

    boolean existsByAccountId(Long Id);
}
