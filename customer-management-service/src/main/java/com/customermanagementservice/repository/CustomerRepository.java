package com.customermanagementservice.repository;

import com.customermanagementservice.models.Customers;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customers, Long> {
    boolean existsByCustomerId(Long id);
}
