package com.customermanagementservice.models;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "CUSTOMER_TABLE")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class Customers {


    /**
     * Customer unique identifier
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CUSTOMER_ID")
    private Long customerId;

    /**
     * Customer Names
     */

    @Column(name = "CUSTOMER_FNAME")
    private String firstName;

    @Column(name = "CUSTOMER_LNAME")
    private String lastName;
}
