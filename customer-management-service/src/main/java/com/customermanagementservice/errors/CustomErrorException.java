package com.customermanagementservice.errors;

public class CustomErrorException extends Exception{
    public CustomErrorException(String message){
        super(message);
    }
}
